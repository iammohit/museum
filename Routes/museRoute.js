const Joi = require('joi');
let UniversalFunctions = require('../Utils/UniversalFunctions');
let Config = require('../Config');
let logger = require('../Config/logger');
const request = require('request');
let moment = require('moment');
module.exports = [
    {
        method: 'GET',
        path: '/api/visitors',
        handler: async  (request, h) =>{
            let {date,ignore} = request.query;
            let month,year;
            try {
                let dateTime = moment(date);
                let finalData = {};
                if(dateTime.isValid() === false){
                    return Promise.reject(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_DATE)

                }
                else {
                    month = dateTime.format('MMM');
                    year = dateTime.year();
                    dateTime = moment(dateTime).format('YYYY-MM-DD');
                    let options = {
                        'method': 'GET',
                        'url': 'https://data.lacity.org/resource/trxm-jn3c.json',
                        qs: { month: dateTime },
                    };
                    let visitorData = await thirdParty(options);
                    let max=-1,min=-1,highest={},lowest = {},ignored={};
                    let total = 0;
                    if(visitorData.length){
                        visitorData = Object.entries(visitorData[0]);
                        visitorData.splice(0,1);
                        for(let i=0;i<visitorData.length;i++){
                           let temp = visitorData[i];
                           if(temp[0]!== ignore) {
                               total = total + parseInt(temp[1]);
                               if (parseInt(temp[1]) > max) {
                                   highest = {
                                       museum: temp[0],
                                       visitors: temp[1]
                                   };
                                   max = temp[1]
                               }
                               if (parseInt(temp[1]) < min || min === -1) {
                                   lowest = {
                                       museum: temp[0],
                                       visitors: temp[1]
                                   };
                                   min = temp[1]
                               }
                           }
                           else {
                               ignored ={
                                   museum: temp[0],
                                   visitors: temp[1]
                               };
                           }
                        }

                    }
                    finalData.attendance={
                        month:month,
                        year:year,
                        highest:highest,
                        lowest:lowest,
                        ignored:ignored,
                        total:total
                    };
                    return UniversalFunctions.sendSuccess(Config.APP_CONSTANTS.STATUS_MSG.SUCCESS.DEFAULT,finalData)

                }
            }
            catch (err) {
                console.log("==========",err);
                return UniversalFunctions.sendError(err)
            }
        },
        config: {
            tags: ['api', 'words'],
            validate: {
                query: Joi.object({
                    date : Joi.number().required(),
                    ignore:Joi.string()
                }),
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                      payloadType: 'form',
                    responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
 ];


async function thirdParty(options) {
    return new Promise((resolve, reject) => {
        request(
            options, function (error, response, body) {
                //console.log("sdds", error, body);
                if (error) {
                    resolve()
                }
                else {
                    body=body?JSON.parse(body):[];
                    resolve(body)
                }
            });
    });
}
